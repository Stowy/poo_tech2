﻿using System.Drawing;
using System.Numerics;

namespace WF_Control
{
    public static class VectorExtensions
    {
        public static PointF ToPointF(this Vector2 vector2)
        {
            return new PointF(vector2.X, vector2.Y);
        }
    }
}