﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Numerics;
using System.Threading.Tasks;

namespace WF_Control
{
    internal class Scene : Control
    {
        private const int FPS = 60;
        private const int PARTICLE_OFFSET_X = 300;
        private const int PARTICLE_OFFSET_Y = 300;
        private const int PARTICLE_ZONE_SIZE = 50;
        private const int MIN_SPEED = -50;
        private const int MAX_SPEED = 50;

        private Bitmap bitmap = null;
        private Graphics g = null;
        private readonly Timer tmrFrame;
        private bool disposed = false;

        private readonly List<Sprite> particles;
        private readonly Random rnd;
        //private readonly Bullet bullet;

        public Scene() : base()
        {
            DoubleBuffered = true;
            rnd = new Random();
            particles = new List<Sprite>();

            Parallel.For(0, 500, i =>
            {
                Vector2 startPosition = new Vector2(
                    rnd.Next(0, PARTICLE_ZONE_SIZE) + PARTICLE_OFFSET_X,
                    rnd.Next(0, PARTICLE_ZONE_SIZE) + PARTICLE_OFFSET_Y);
                Vector2 speed = new Vector2(rnd.Next(MIN_SPEED, MAX_SPEED), rnd.Next(MIN_SPEED, MAX_SPEED));
                //Sprite sprite = new Sprite(startPosition, new Size(10, 10), speed);
                //particles.Add(sprite);
                //Paint += sprite.Paint;

                Bullet bullet = new Bullet(startPosition, new Size(10, 10), speed, 200);
                particles.Add(bullet);
                Paint += bullet.Paint;
            });

            //bullet = new Bullet(new Vector2(200, 200), new Size(10, 10), new Vector2(50, -50), 200);
            //Paint += bullet.Paint;

            tmrFrame = new Timer()
            {
                Interval = 1000 / FPS,
                Enabled = true
            };
            tmrFrame.Tick += TmrFrame_Tick;
        }

        private void TmrFrame_Tick(object sender, System.EventArgs e) => Invalidate();

        protected override void OnPaint(PaintEventArgs e)
        {
            // Initialize bitmap and g if null
            bitmap ??= new Bitmap(Size.Width, Size.Height);
            g ??= Graphics.FromImage(bitmap);

            PaintEventArgs p = new PaintEventArgs(g, e.ClipRectangle);

            p.Graphics.Clear(BackColor);
            base.OnPaint(p);
            e.Graphics.DrawImage(bitmap, new Point(0, 0));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Dispose managed objects
                bitmap.Dispose();
                g.Dispose();
                tmrFrame.Dispose();
            }

            // Free unmanaged resources
            // Set large fields to null
            disposed = true;

            base.Dispose(disposing);
        }

        ~Scene() => Dispose(false);
    }
}