﻿using System;
using System.Drawing;
using System.Numerics;
using System.Windows.Forms;

namespace WF_Control
{
    class Bullet : Sprite
    {
        private const float G = 9.81f;
        private readonly float timeFactor;

        public Bullet(Vector2 startPosition, Size size, Vector2 speed, float timeFactor) : base(startPosition, size, speed)
        {
            this.timeFactor = timeFactor;
        }

        public override Vector2 Position
        {
            get
            {
                float elapsedTime = sw.ElapsedMilliseconds / timeFactor;
                float x = speed.X * elapsedTime + startPosition.X;
                float y = 0.5f * G * (float)Math.Pow(elapsedTime, 2) + speed.Y * elapsedTime + startPosition.Y;
                return new Vector2(x, y);
            }
        }

        public override void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.Red, new Rectangle(Point.Round(Position.ToPointF()), Size));
        }
    }
}
