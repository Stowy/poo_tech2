﻿// <copyright file="EnumExtensions.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_MicroMouse
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Extensions for flags enum.
    /// Taken here : https://stackoverflow.com/questions/93744/most-common-c-sharp-bitwise-operations-on-enums.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Checks if the enum has a certain flag.
        /// </summary>
        /// <typeparam name="T">Type of the value parameter.</typeparam>
        /// <param name="type">Enum to check.</param>
        /// <param name="value">Value to check in the enum.</param>
        /// <returns>Returns true if the enum has the provided flag.</returns>
        /// <example><code>
        /// SomeType value = SomeType.Grapes;
        /// bool hasGrapes = value.Has(SomeType.Grapes); //true
        /// </code></example>
        [SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Reviewed.")]
        public static bool Has<T>(this Enum type, T value)
        {
            try
            {
                return ((int)(object)type & (int)(object)value) == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the enum equals to the value.
        /// </summary>
        /// <typeparam name="T">Type of the value.</typeparam>
        /// <param name="type">Enum to check.</param>
        /// <param name="value">Value to compare to the enum.</param>
        /// <returns>Returns true if the enum is equal to the value.</returns>
        /// <example><code>
        /// SomeType value = SomeType.Grapes;
        /// bool isGrapes = value.Is(SomeType.Grapes); //true
        /// </code></example>
        [SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Reviewed.")]
        public static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (int)(object)type == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Adds the provided flag to the enum.
        /// </summary>
        /// <typeparam name="T">Type of the value.</typeparam>
        /// <param name="type">Enum to modify.</param>
        /// <param name="value">Value to add in the enum.</param>
        /// <returns>Returns the modified enum.</returns>
        /// <example><code>
        /// SomeType value = SomeType.Grapes;
        /// value = value.Add(SomeType.Oranges);
        /// </code></example>
        /// <exception cref="ArgumentException">Throws if the value can't be appended.</exception>"
        public static T Add<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type | (int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not append value from enumerated type '{0}'.",
                        typeof(T).Name), ex);
            }
        }

        /// <summary>
        /// Removes a specified flag to the enum.
        /// </summary>
        /// <typeparam name="T">Type of value.</typeparam>
        /// <param name="type">Enum to modify.</param>
        /// <param name="value">Value to remove in the enum.</param>
        /// <returns>Returns the modified enum.</returns>
        /// <example><code>
        /// SomeType value = SomeType.Grapes;
        /// value = value.Remove(SomeType.Grapes);
        /// </code></example>
        /// <exception cref="ArgumentException">Throws if the value can't be removed.</exception>"
        public static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type & ~(int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not remove value from enumerated type '{0}'.",
                        typeof(T).Name), ex);
            }
        }
    }
}
