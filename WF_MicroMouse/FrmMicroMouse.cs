﻿// <copyright file="FrmMicroMouse.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_MicroMouse
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using WF_MicroMouse.Properties;

    /// <summary>
    /// View of the MicroMouse project.
    /// </summary>
    public partial class FrmMicroMouse : Form
    {
        private readonly Maze maze;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrmMicroMouse"/> class.
        /// </summary>
        public FrmMicroMouse()
        {
            this.InitializeComponent();
            this.maze = new Maze(Resources.at135)
            {
                Location = new Point(50, 50),
                Name = "Maze",
                Size = new Size(1000, 1000),
            };
            this.Controls.Add(this.maze);
        }
    }
}
