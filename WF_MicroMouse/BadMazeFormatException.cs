﻿// <copyright file="BadMazeFormatException.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_MicroMouse
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception for when a provided maze is not of the good format.
    /// </summary>
    [Serializable]
    public class BadMazeFormatException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BadMazeFormatException"/> class.
        /// </summary>
        public BadMazeFormatException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadMazeFormatException"/> class.
        /// </summary>
        /// <param name="message">Message for the exception.</param>
        public BadMazeFormatException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadMazeFormatException"/> class.
        /// </summary>
        /// <param name="message">Message for the exception.</param>
        /// <param name="innerException">Inner exception.</param>
        public BadMazeFormatException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BadMazeFormatException"/> class.
        /// </summary>
        /// <param name="serializationInfo">Serialization informations.</param>
        /// <param name="streamingContext">Streaming context.</param>
        protected BadMazeFormatException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}
