﻿// <copyright file="Maze.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_MicroMouse
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    /// <summary>
    /// Represents a maze like the Micromouse event.
    /// </summary>
    public class Maze : Control
    {
        /// <summary>
        /// Size of a cell when it's drawn.
        /// </summary>
        public const int CellSize = 30;

        private readonly WallFlags[,] cells;
        private readonly int mazeWidth;
        private Bitmap bitmap;
        private Graphics g;

        /// <summary>
        /// Initializes a new instance of the <see cref="Maze"/> class.
        /// </summary>
        /// <param name="cells">Array of cells.</param>
        public Maze(WallFlags[,] cells)
        {
            this.cells = cells;
            this.mazeWidth = (int)Math.Sqrt(this.cells.Length);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Maze"/> class.
        /// </summary>
        /// <param name="mazeText">Text representation of a maze.</param>
        public Maze(string mazeText)
        {
            this.cells = this.FromText(mazeText);
            this.mazeWidth = (int)Math.Sqrt(this.cells.Length);
        }

        /// <summary>
        /// Flags for the position of the wall.
        /// </summary>
        [Flags]
        public enum WallFlags
        {
            /// <summary>
            /// Flag if there's a wall on the north.
            /// </summary>
            North = 0b0001,

            /// <summary>
            /// Flag if there's a wall on the east.
            /// </summary>
            East = 0b0010,

            /// <summary>
            /// Flag if there's a wall on the south.
            /// </summary>
            South = 0b0100,

            /// <summary>
            /// Flag if there's a wall on the west.
            /// </summary>
            West = 0b1000,
        }

        /// <inheritdoc/>
        protected override void OnPaint(PaintEventArgs e)
        {
            this.bitmap ??= new Bitmap(this.Size.Width, this.Size.Height);
            this.g ??= Graphics.FromImage(this.bitmap);

            PaintEventArgs p = new PaintEventArgs(this.g, e.ClipRectangle);

            p.Graphics.Clear(this.BackColor);
            this.DrawMaze(p);
            base.OnPaint(p);
            e.Graphics.DrawImage(this.bitmap, this.Location);
        }

        /// <summary>
        /// Draws the maze.
        /// </summary>
        /// <param name="p">Event args of the paint.</param>
        private void DrawMaze(PaintEventArgs p)
        {
            for (int x = 0; x < this.mazeWidth; x++)
            {
                for (int y = 0; y < this.mazeWidth; y++)
                {
                    int posX = x * CellSize;
                    int posY = y * CellSize;
                    WallFlags cell = this.cells[x, y];
                    if (cell.Has(WallFlags.North))
                    {
                        p.Graphics.DrawLine(Pens.Black, new Point(posX, posY), new Point(posX + CellSize, posY));
                    }

                    if (cell.Has(WallFlags.South))
                    {
                        p.Graphics.DrawLine(Pens.Black, new Point(posX, posY + CellSize), new Point(posX + CellSize, posY + CellSize));
                    }

                    if (cell.Has(WallFlags.East))
                    {
                        p.Graphics.DrawLine(Pens.Black, new Point(posX + CellSize, posY), new Point(posX + CellSize, posY + CellSize));
                    }

                    if (cell.Has(WallFlags.West))
                    {
                        p.Graphics.DrawLine(Pens.Black, new Point(posX, posY), new Point(posX, posY + CellSize));
                    }
                }
            }
        }

        private WallFlags[,] FromText(string mazeText)
        {
            WallFlags test = 0;
            test = test.Add(WallFlags.North);
            test = test.Add(WallFlags.South);
            string[] splittedMaze = mazeText.Split('\n');
            string firstLine = splittedMaze[0];
            int charPerCells = 0;

            // The first character we see is the post
            char postChar = firstLine[0];

            // The next character is a horizontal wall
            char hWallChar = firstLine[1];

            int mazeWidth = firstLine.Count(c => c == postChar) - 1;

            // There must be at least 2 chars per cell plus the last post
            if (firstLine.Length < (mazeWidth * 2) + 1)
            {
                throw new BadMazeFormatException("The provided maze text doesn't have a good format.");
            }

            if (hWallChar == ' ')
            {
                throw new BadMazeFormatException("The provided maze text doesn't have a good format.");
            }

            // Check the next postChar to see how many characters we have per cell
            if (firstLine[2] == postChar)
            {
                charPerCells = 2;
            }
            else if (firstLine[3] == postChar)
            {
                charPerCells = 3;
            }
            else if (firstLine[4] == postChar)
            {
                charPerCells = 4;
            }
            else
            {
                throw new BadMazeFormatException("The provided maze text doesn't have a good format.");
            }

            // Check the char for the vertical wall, it's the first one on the second line
            string secondLine = splittedMaze[1];
            char vWallChar = secondLine[0];
            if (vWallChar == ' ')
            {
                throw new BadMazeFormatException("The provided maze text doesn't have a good format.");
            }

            // now we just assume the rest of the file makes sense in the same pattern
            // until an error occurs
            WallFlags[,] newMaze = new WallFlags[mazeWidth, mazeWidth];

            // a text maze starts top left and every row takes up two lines of text
            for (int x = 0; x < mazeWidth - 1; x++)
            {
                string lineHWalls = splittedMaze[x * 2];
                string lineVWalls = splittedMaze[(x * 2) + 1];
                for (int y = 0; y < mazeWidth; y++)
                {
                    if (lineHWalls[(charPerCells * y) + 1] == hWallChar)
                    {
                        newMaze[x, y] = newMaze[x, y].Add(WallFlags.North);
                    }

                    if (lineVWalls[charPerCells * y] == vWallChar)
                    {
                        newMaze[x, y] = newMaze[x, y].Add(WallFlags.West);
                    }

                    // fill east wall at the end of the row
                    if (lineVWalls[charPerCells * mazeWidth] == vWallChar)
                    {
                        newMaze[mazeWidth - 1, y] = newMaze[mazeWidth - 1, y].Add(WallFlags.East);
                    }
                }
            }

            // Take the last line that is not an empty new line
            string lastLine = splittedMaze[mazeWidth * 2];

            // there should be one row of text left which is the south walls of the maze
            for (int x = 0; x < mazeWidth; x++)
            {
                if (lastLine[1 + (charPerCells * x)] == hWallChar)
                {
                    newMaze[x, mazeWidth - 1] = newMaze[x, mazeWidth - 1].Add(WallFlags.South);
                }
            }

            return newMaze;
        }
    }
}
