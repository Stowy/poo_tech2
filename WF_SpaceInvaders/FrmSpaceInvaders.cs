﻿// <copyright file="FrmSpaceInvaders.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Main form for the space invaders game.
    /// </summary>
    public partial class FrmSpaceInvaders : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrmSpaceInvaders"/> class.
        /// </summary>
        public FrmSpaceInvaders()
        {
            InitializeComponent();
            Load += spaceInvadersScene.OnLoad;
        }
    }
}
