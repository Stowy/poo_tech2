﻿// <copyright file="SpaceInvadersScene.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System;
    using System.Drawing;
    using System.Numerics;
    using System.Windows.Forms;

    /// <summary>
    /// Represents the scene of the space invaders game.
    /// </summary>
    public class SpaceInvadersScene : Scene
    {
        private const float ShipSpeed = 200;

        private readonly Label mainTitle;
        private Ship ship;
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpaceInvadersScene"/> class.
        /// </summary>
        public SpaceInvadersScene()
            : base()
        {
            mainTitle = new Label
            {
                Text = $"Space Invaders{Environment.NewLine}Click to start",
                AutoSize = false,
                Location = new Point(0, 0),
                ForeColor = Color.White,
                Font = new Font(new FontFamily("Consolas"), 30),
                TextAlign = ContentAlignment.MiddleCenter,
            };
            Click += SpaceInvadersScene_Click;
            mainTitle.Click += SpaceInvadersScene_Click;
            Controls.Add(mainTitle);
            GameManager.Instance.State = GameState.MainTitle;
        }

        /// <summary>
        /// Loads the scene.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments of the event.</param>
        public void OnLoad(object sender, EventArgs e)
        {
            mainTitle.Width = Width;
            mainTitle.Height = Convert.ToInt32(Height * 0.4);
        }

        /// <summary>
        /// Loads the first level.
        /// </summary>
        public void LoadLevelOne()
        {
            if (GameManager.Instance.State != GameState.LevelOne)
            {
                mainTitle.Visible = false;
                ship = new Ship(new Vector2(50, Height - 50), new Size(30, 30), 3, new Vector2(ShipSpeed, 0), Color.FromArgb(255, 255, 255));
                KeyDown += ship.OnKeyDown;
                Paint += ship.Paint;
                GameManager.Instance.State = GameState.LevelOne;
            }
        }

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                // Dispose managed objects
                mainTitle.Dispose();
            }

            // Free unmanaged resources
            // Set large fields to null
            disposed = true;

            base.Dispose(disposing);
        }

        private void SpaceInvadersScene_Click(object sender, EventArgs e)
        {
            LoadLevelOne();
        }
    }
}
