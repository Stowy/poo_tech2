﻿// <copyright file="Scene.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// A scene class for the space invader.
    /// </summary>
    public class Scene : Control
    {
        private const int FPS = 60;

        private readonly Timer tmrFrame;
        private Bitmap bitmap = null;
        private Graphics g = null;
        private bool disposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class.
        /// </summary>
        public Scene()
            : base()
        {
            DoubleBuffered = true;
            tmrFrame = new Timer()
            {
                Interval = 1000 / FPS,
                Enabled = true,
            };
            tmrFrame.Tick += TmrFrame_Tick;
        }

        /// <inheritdoc/>
        protected override void OnPaint(PaintEventArgs e)
        {
            // Initialize bitmap and g if null
            bitmap ??= new Bitmap(Size.Width, Size.Height);
            g ??= Graphics.FromImage(bitmap);

            PaintEventArgs p = new PaintEventArgs(g, e.ClipRectangle);

            p.Graphics.Clear(BackColor);
            base.OnPaint(p);
            e.Graphics.DrawImage(bitmap, new Point(0, 0));
        }

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                // Dispose managed objects
                bitmap.Dispose();
                g.Dispose();
                tmrFrame.Dispose();
            }

            // Free unmanaged resources
            // Set large fields to null
            disposed = true;

            base.Dispose(disposing);
        }

        private void TmrFrame_Tick(object sender, EventArgs e) => Invalidate();
    }
}