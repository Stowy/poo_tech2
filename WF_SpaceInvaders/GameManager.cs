﻿// <copyright file="GameManager.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System;

    /// <summary>
    /// State of the game.
    /// </summary>
    public enum GameState
    {
        /// <summary>
        /// The game is on the main title.
        /// </summary>
        MainTitle,

        /// <summary>
        /// The game is on the first level.
        /// </summary>
        LevelOne,
    }

    /// <summary>
    /// Singleton that manages the state of the game.
    /// </summary>
    public class GameManager
    {
        /// <summary>
        /// Lazy class instance.
        /// </summary>
        public static readonly Lazy<GameManager> Lazy = new Lazy<GameManager>(() => new GameManager());

        private GameManager()
        {
        }

        /// <summary>
        /// Gets the instance of the game manager.
        /// </summary>
        public static GameManager Instance
        {
            get
            {
                return Lazy.Value;
            }
        }

        /// <summary>
        /// Gets or sets the current state of the game manager.
        /// </summary>
        internal GameState State { get; set; }
    }
}
