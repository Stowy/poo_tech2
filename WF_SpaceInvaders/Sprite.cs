﻿// <copyright file="Sprite.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System.Diagnostics;
    using System.Drawing;
    using System.Numerics;
    using System.Windows.Forms;

    /// <summary>
    /// Represents a moving sprite.
    /// </summary>
    public class Sprite
    {
        private const float DefaultPositionX = 50;
        private const float DefaultPositionY = 50;
        private const int DefaultHeight = 50;
        private const int DefaultWidth = 50;
        private const float DefaulSpeedX = 10f;
        private const float DefaultSpeedY = 10f;
        private const int DefaultColorR = 0;
        private const int DefaultColorG = 0;
        private const int DefaultColorB = 0;

        private readonly Stopwatch sw;
        private readonly Color color;
        private Vector2 speed;
        private Vector2 startPosition;

        /// <summary>
        /// Initializes a new instance of the <see cref="Sprite"/> class.
        /// </summary>
        /// <param name="startPosition">Start point of the sprite.</param>
        /// <param name="size">Size in pixel of the sprite.</param>
        /// <param name="speed">Speed in pixel/second of the sprite.</param>
        /// <param name="color">Color of the sprite.</param>
        public Sprite(Vector2 startPosition, Size size, Vector2 speed, Color color)
        {
            this.startPosition = startPosition;
            Size = size;
            this.speed = speed;
            this.color = color;
            sw = Stopwatch.StartNew();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sprite"/> class.
        /// </summary>
        public Sprite()
            : this(
                new Vector2(DefaultPositionX, DefaultPositionY),
                new Size(DefaultWidth, DefaultHeight),
                new Vector2(DefaulSpeedX, DefaultSpeedY),
                Color.FromArgb(DefaultColorR, DefaultColorG, DefaultColorB))
        {
        }

        /// <summary>
        /// Gets the current position of the sprite.
        /// </summary>
        public virtual Vector2 Position
        {
            get
            {
                float elapsedTime = sw.ElapsedMilliseconds / 1000f;
                return startPosition + (elapsedTime * speed);
            }
        }

        /// <summary>
        /// Gets the size of the sprite.
        /// </summary>
        public Size Size { get; private set; }

        /// <summary>
        /// Gets the color of the sprite.
        /// </summary>
        protected Color Color => color;

        /// <summary>
        /// Paints the sprite.
        /// </summary>
        /// <param name="sender">Sender that invoked the paint event.</param>
        /// <param name="e">Arguments of the paint event.</param>
        public virtual void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Color), new Rectangle(Position.ToPoint(), Size));
        }
    }
}