﻿// <copyright file="Ship.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System.Drawing;
    using System.Numerics;
    using System.Windows.Forms;

    /// <summary>
    /// Ship class. Descends from sprite.
    /// </summary>
    public class Ship : Sprite
    {
        private readonly int width;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        public Ship()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        /// <param name="startPosition">Start position of the ship.</param>
        /// <param name="size">Size of the ship.</param>
        /// <param name="width">Width of the ship.</param>
        /// <param name="speed">Speed in pixel/s of the ship.</param>
        /// <param name="color">Color of the ship.</param>
        public Ship(Vector2 startPosition, Size size, int width, Vector2 speed, Color color)
            : base(startPosition, size, speed, color)
        {
            this.width = width;
        }

        /// <inheritdoc/>
        public override Vector2 Position => base.Position;

        /// <summary>
        /// Event triggered when a key is pressed.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event args.</param>
        public void OnKeyDown(object sender, KeyEventArgs e)
        {
        }

        /// <inheritdoc/>
        public override void Paint(object sender, PaintEventArgs e)
        {
            // Compute the vertices of the ship
            int halfHeight = Size.Height / 2;
            int halfWidth = Size.Width / 2;

            Vector2 vertex1 = new Vector2(Position.X, Position.Y - halfHeight);
            Vector2 vertex2 = new Vector2(Position.X - halfWidth, Position.Y + halfHeight);
            Vector2 vertex3 = new Vector2(Position.X + halfWidth, Position.Y + halfHeight);

            // Draw the lines
            Pen pen = new Pen(Color, width);
            e.Graphics.DrawLine(pen, vertex1.ToPoint(), vertex2.ToPoint());
            e.Graphics.DrawLine(pen, vertex3.ToPoint(), vertex2.ToPoint());
            e.Graphics.DrawLine(pen, vertex1.ToPoint(), vertex3.ToPoint());
        }
    }
}
