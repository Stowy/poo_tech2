﻿// <copyright file="VectorExtensions.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System.Drawing;
    using System.Numerics;

    /// <summary>
    /// Extension methods for the vector class.
    /// </summary>
    public static class VectorExtensions
    {
        /// <summary>
        /// Converts to PointF a Vector2.
        /// </summary>
        /// <param name="vector2">Vector2 to convert.</param>
        /// <returns>Returns the converted Vector2.</returns>
        public static PointF ToPointF(this Vector2 vector2)
        {
            return new PointF(vector2.X, vector2.Y);
        }

        /// <summary>
        /// Converts a Vector2 to a Point.
        /// </summary>
        /// <param name="vector2">Vector2 to convert.</param>
        /// <returns>Returns the converted vector.</returns>
        public static Point ToPoint(this Vector2 vector2)
        {
            return Point.Round(vector2.ToPointF());
        }
    }
}