﻿// <copyright file="Program.cs" company="CFPT">
// Copyright (c) CFPT. All rights reserved.
// </copyright>

namespace WF_SpaceInvaders
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Main program.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmSpaceInvaders());
        }
    }
}
